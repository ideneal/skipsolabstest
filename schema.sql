/* Default schema */

DROP DATABASE IF EXISTS skipsolabs;
CREATE DATABASE IF NOT EXISTS skipsolabs;
USE skipsolabs;

CREATE TABLE User (
    Id INT NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(60) NOT NULL,
    LastName VARCHAR(60) NOT NULL,
    Email VARCHAR(60) NOT NULL,
    Username VARCHAR(25) NOT NULL,
    PRIMARY KEY(Id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

CREATE TABLE Address (
    Id INT NOT NULL AUTO_INCREMENT,
    Street VARCHAR(60) NOT NULL,
    City VARCHAR(60) NOT NULL,
    ZipCode VARCHAR(25) NOT NULL,
    Country VARCHAR(60) NOT NULL,
    PRIMARY KEY(Id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

CREATE TABLE Company (
    Id INT NOT NULL AUTO_INCREMENT,
    Name VARCHAR(60) NOT NULL,
    Description VARCHAR(200) NOT NULL,
    PRIMARY KEY(Id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

CREATE TABLE UserAddress (
    UserId INT NOT NULL,
    AddressId INT NOT NULL,
    PRIMARY KEY (UserId, AddressId),
    FOREIGN KEY (UserId) REFERENCES User(Id),
    FOREIGN KEY (AddressId) REFERENCES Address(Id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

CREATE TABLE UserCompany (
    UserId INT NOT NULL,
    CompanyId INT NOT NULL,
    PRIMARY KEY (UserId, CompanyId),
    FOREIGN KEY (UserId) REFERENCES User(Id),
    FOREIGN KEY (CompanyId) REFERENCES Company(Id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;



/* Retrieve all Users and Companies */

SELECT *
FROM User
      INNER JOIN UserCompany AS UC on User.Id = UC.UserId
      INNER JOIN Company AS CO on UC.CompanyId = CO.Id
;


/* Given a company, return all Users associated */

SELECT *
FROM User
WHERE Id IN ( SELECT UserId
              FROM UserCompany
              INNER JOIN Company C2 on UserCompany.CompanyId = C2.Id
              WHERE C2.Name = 'Google'
              )