<?php

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use App\App;


$request  = Request::createFromGlobals();
$response = App::handleRequest($request);

$response->send();