# Skipsolabs Test #

### Installation ###

Install all packages by typing:

```
composer install
```

At the end you have to change the Homestead.yaml for your needs,
add `192.168.10.21 skipsolabs.test` in your hosts file and type the following:

```
vagrant up
```

You can now browse to [home](http://skipsolabs.test).

For the admin page you must go to [admin](http://skipsolabs.test/admin).