<?php

namespace App;


use App\Controller\AdminController;
use App\Controller\UserController;
use App\View\TemplateEngine\TemplateEngineFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class App
{
    public static function handleRequest(Request $request): Response
    {
        $templateEngine = TemplateEngineFactory::create([__DIR__ . '/../templates'], __DIR__ . '/../tmp/cache');

        switch ($request->getPathInfo()) {
            case '/admin':
                $controller = new AdminController($request, $templateEngine);
                return $controller->index();
            case '/form':
                $controller = new UserController($request, $templateEngine);
                return $controller->form();
            case '/':
            default:
                $controller = new UserController($request, $templateEngine);
                return $controller->index();
        }

    }
}