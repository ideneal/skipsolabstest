<?php

namespace App\Data\Transformer;


use App\Entity\User;

class UserTransformer implements ArrayTransformerInteface
{
    /**
     * @var CompanyTransformer
     */
    private $companyTransformer;

    /**
     * @var AddressTransformer
     */
    private $addressTransformer;

    /**
     * UserTransformer constructor.
     */
    public function __construct()
    {
        $this->companyTransformer = new CompanyTransformer();
        $this->addressTransformer = new AddressTransformer();
    }

    /**
     * @param $user
     *
     * @return array
     * @throws \Exception
     */
    public function toArray($user): array
    {
        if (!$user instanceof User) {
            throw new \Exception('Invalid user');
        }

        $array = [
            'name'     => implode(" ", [$user->getFirstName(), $user->getLastName()]),
            'username' => $user->getUsername(),
            'email'    => $user->getEmail(),
            'address'  => $this->addressTransformer->toArray($user->getAddress()),
        ];

        if ($user->getCompany()) {
            $array['company'] = $this->companyTransformer->toArray($user->getCompany());
        }

        return $array;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function fromArray(array $data)
    {
        $user = new User();

        if (isset($data['name'])) {
            list($firstName, $lastName) = explode(' ', $data['name']);
            $user
                ->setFirstName($firstName)
                ->setLastName($lastName)
            ;
        }

        if (isset($data['username'])) {
            $user->setUsername($data['username']);
        }

        if (isset($data['email'])) {
            $user->setEmail($data['email']);
        }

        if (isset($data['address'])) {
            $address = $this->addressTransformer->fromArray($data['address']);
            $user->setAddress($address);
        }

        if (isset($data['company'])) {
            $company = $this->companyTransformer->fromArray($data['company']);
            $user->setCompany($company);
        }

        return $user;
    }
}