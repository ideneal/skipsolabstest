<?php

namespace App\Data\Transformer;


use App\Entity\Company;

class CompanyTransformer implements ArrayTransformerInteface
{
    /**
     * @param $company
     *
     * @return array
     * @throws \Exception
     */
    public function toArray($company): array
    {
        if (!$company instanceof Company) {
            throw new \Exception('Invalid company');
        }

        return [
            'name'        => $company->getName(),
            'description' => $company->getDescription(),
        ];
    }

    /**
     * @param array $data
     *
     * @return Company
     */
    public function fromArray(array $data)
    {
        $company = new Company();

        if (isset($data['name'])) {
            $company->setName($data['name']);
        }

        if (isset($data['description'])) {
            $company->setDescription($data['description']);
        }

        return $company;
    }
}