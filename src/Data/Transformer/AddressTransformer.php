<?php

namespace App\Data\Transformer;


use App\Entity\Address;

class AddressTransformer implements ArrayTransformerInteface
{
    /**
     * @param $address
     *
     * @return array
     * @throws \Exception
     */
    public function toArray($address): array
    {
        if (!$address instanceof Address) {
            throw new \Exception('Invalid address');
        }

        return [
            'city'    => $address->getCity(),
            'country' => $address->getCountry(),
            'street'  => $address->getStreet(),
            'zipcode' => $address->getZipCode(),
        ];
    }

    /**
     * @param array $data
     *
     * @return Address
     */
    public function fromArray(array $data)
    {
        $address = new Address();
        
        if (isset($data['city'])) {
            $address->setCity($data['city']);
        }

        if (isset($data['country'])) {
            $address->setCountry($data['country']);
        }

        if (isset($data['street'])) {
            $address->setStreet($data['street']);
        }

        if (isset($data['zipcode'])) {
            $address->setZipCode($data['zipcode']);
        }

        return $address;
    }
}