<?php

namespace App\Data\Transformer;


interface ArrayTransformerInteface
{
    public function toArray($data): array;

    public function fromArray(array $data);
}