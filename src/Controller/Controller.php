<?php

namespace App\Controller;

use App\View\TemplateEngine\TemplateEngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class Controller implements ControllerInterface
{
    protected $request;

    private $templateEngine;

    public function __construct(Request $request, TemplateEngineInterface $templateEngine)
    {
        $this->request        = $request;
        $this->templateEngine = $templateEngine;
    }

    public function index(): Response
    {
    }

    public function render(string $view, array $parameters = []): Response
    {
        $template = $this->templateEngine->render($view, $parameters);
        return new Response($template);
    }

    public function getFormParameters(): array
    {
        $content    = $this->request->getContent();
        $parameters = [];

        $couples = explode('&', $content);

        foreach ($couples as $couple) {
            list($key, $value) = explode('=', $couple);
            $parameters[urldecode($key)] = urldecode($value);
        }

        return $parameters;
    }
}