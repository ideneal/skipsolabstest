<?php

namespace App\Controller;


use App\Data\Transformer\UserTransformer;
use App\Form\Validator\UserValidator;
use App\Service\TypicodeService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function index(): Response
    {
        return $this->render('user/index.html.twig');
    }

    public function form(): JsonResponse
    {
        $userTransformer = new UserTransformer();
        $userValidator   = new UserValidator();
        $userService     = TypicodeService::getInstance();

        $data = json_decode($this->request->getContent(), true);
        $errors = $userValidator->validate($data);

        if ($errors) {
            return new JsonResponse(['errors' => $errors], 500);
        }

        $user = $userTransformer->fromArray($data);

        $result = $userService->addUser($user);

        $statusCode = $result ? 200 : 500;
        $message    = $result ? 'Ok' : 'Error';

        return new JsonResponse(['message' => $message], $statusCode);
    }
}