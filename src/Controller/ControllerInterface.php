<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;

interface ControllerInterface
{
    public function render(string $view, array $parameters = []): Response;
}