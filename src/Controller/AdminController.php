<?php

namespace App\Controller;


use App\Service\TypicodeService;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public function index(): Response
    {
        $userService = TypicodeService::getInstance();
        $users = $userService->getUsers();

        return $this->render('admin/index.html.twig', ['users' => $users]);
    }
}