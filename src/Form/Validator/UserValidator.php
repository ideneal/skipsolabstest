<?php

namespace App\Form\Validator;


class UserValidator implements ValidatorInterface
{
    /**
     * @var AddressValidator
     */
    private $addressValidator;

    /**
     * UserValidator constructor.
     */
    public function __construct()
    {
        $this->addressValidator = new AddressValidator();
    }

    /**
     * Returns an array of error after data validation.
     *
     * @param $data
     *
     * @return array
     */
    public function validate($data): array
    {
        $errors = [];

        if (isset($data['name'])) {
            $re = '/^([^0-9]*)$/m';
            preg_match_all($re, $data['name'], $matches, PREG_SET_ORDER, 0);

            if (!$matches) {
                $errors['name'] = 'The name must not contains numbers';
            }
        }

        if (isset($data['username'])) {
            $re = '/^[\w]{8,20}$/m';
            preg_match_all($re, $data['username'], $matches, PREG_SET_ORDER, 0);

            if (!$matches) {
                $errors['username'] = 'The username must contains only letters and numbers and must be between 8 and 20 characters';
            }
        }

        if (isset($data['email'])) {
            $re = '/^[\w.%+-]+@[\w.-]+\.[a-zA-Z]{2,}$/m';
            preg_match_all($re, $data['email'], $matches, PREG_SET_ORDER, 0);

            if (!$matches) {
                $errors['email'] = 'Invalid email address';
            }
        }

        if (isset($data['address'])) {
            $address = $data['address'];
            $addressErrors = $this->addressValidator->validate($address);

            if ($addressErrors) {
                $errors['address'] = $addressErrors;
            }
        }

        return $errors;
    }
}