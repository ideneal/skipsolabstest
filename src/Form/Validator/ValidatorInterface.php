<?php

namespace App\Form\Validator;


interface ValidatorInterface
{
    /**
     * Returns an array of error after data validation.
     *
     * @param $data
     *
     * @return array
     */
    public function validate($data): array;
}