<?php

namespace App\View\TemplateEngine;


abstract class TemplateEngine implements TemplateEngineInterface
{
    /**
     * @var array
     */
    protected $paths;

    /**
     * @var string
     */
    protected $cachePath;

    /**
     * TemplateEngine constructor.
     *
     * @param array       $paths
     * @param string|null $cachePath
     */
    public function __construct(array $paths, ?string $cachePath)
    {
        $this->paths     = $paths;
        $this->cachePath = $cachePath;
    }
}