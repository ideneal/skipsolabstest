<?php

namespace App\View\TemplateEngine;


interface TemplateEngineInterface
{
    /**
     * Render a template
     *
     * @param string $view
     * @param array  $parameters
     *
     * @return string
     */
    public function render(string $view, array $parameters = []): string;
}