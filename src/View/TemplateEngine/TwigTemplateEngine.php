<?php

namespace App\View\TemplateEngine;


class TwigTemplateEngine extends TemplateEngine
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * TwigTemplateEngine constructor.
     *
     * @param array       $paths
     * @param string|null $cachePath
     */
    public function __construct(array $paths, ?string $cachePath)
    {
        parent::__construct($paths, $cachePath);

        $loader = new \Twig_Loader_Filesystem($this->paths);
        $this->twig = new \Twig_Environment($loader, [
            'cache' => $this->cachePath,
        ]);

        $lexer = new \Twig_Lexer($this->twig, [
            'tag_comment'   => ['{#', '#}'],
            'tag_block'     => ['{%', '%}'],
            'tag_variable'  => ['{{-', '-}}'],
            'interpolation' => ['#{', '}'],
        ]);

        $this->twig->setLexer($lexer);
    }

    /**
     * Render a template
     *
     * @param string $view
     * @param array  $parameters
     *
     * @return string
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render(string $view, array $parameters = []): string
    {
        return $this->twig->render($view, $parameters);
    }
}