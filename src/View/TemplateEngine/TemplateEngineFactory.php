<?php

namespace App\View\TemplateEngine;


class TemplateEngineFactory
{
    public static function create(array $paths, ?string $cachePath): TemplateEngineInterface
    {
        return new TwigTemplateEngine($paths, $cachePath);
    }
}