<?php

namespace App\Service;


use App\Entity\User;
use App\Data\Transformer\UserTransformer;
use GuzzleHttp\Client;

class TypicodeService
{
    /**
     * @var TypicodeService
     */
    protected static $instance;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var UserTransformer
     */
    private $userTransformer;

    protected function __construct()
    {
        $this->userTransformer = new UserTransformer();
        $this->client          = new Client([
            'base_uri' => 'https://jsonplaceholder.typicode.com/',
        ]);
    }

    /**
     * Get current instance
     *
     * @return TypicodeService
     */
    public static function getInstance(): TypicodeService
    {
        if (!static::$instance) {
            static::$instance = new TypicodeService();
        }

        return static::$instance;
    }

    /**
     * @param User $user
     *
     * @return bool
     * @throws \Exception
     */
    public function addUser(User $user)
    {
        $response = $this->client->post('users', [
            'json' => $this->userTransformer->toArray($user),
        ]);

        return $response->getStatusCode() === 201;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $response = $this->client->get('http://jsonplaceholder.typicode.com/users');
        $items = json_decode((string)$response->getBody(), true);
        $users = [];

        foreach ($items as $item) {
            $users[] = $this->userTransformer->fromArray($item);
        }

        return $users;
    }
}